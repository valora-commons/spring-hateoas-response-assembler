package dev.valora.commons.springhateoasresponseassembler;

import java.util.Collection;
import java.util.Collections;

import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.EmbeddedWrappers;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public abstract class HateoasResponseAssembler<S, D extends RepresentationModel<D>>
		extends RepresentationModelAssemblerSupport<S, D> {

	private PagedResourcesAssembler<S> pagedResourcesAssembler;

	protected HateoasResponseAssembler(PagedResourcesAssembler<S> pagedResourcesAssembler, Class<?> controllerClass, Class<D> resourceType) {
		super(controllerClass, resourceType);
		this.pagedResourcesAssembler = pagedResourcesAssembler;
	}

	@SuppressWarnings("unchecked")
	public PagedModel<D> toPagedModel(Page<S> page) {
		if (page == null || !page.hasContent()) {
			return (PagedModel<D>) pagedResourcesAssembler.toEmptyModel(Page.empty(), getResourceType());
		}
		return pagedResourcesAssembler.toModel(page, this);
	}

	@Override
	@SuppressWarnings("unchecked")
	public CollectionModel<D> toCollectionModel(Iterable<? extends S> entities) {
		if (entities == null || !entities.iterator().hasNext()) {
			return (CollectionModel<D>) CollectionModel.of(emptyCollection());
		}
		return super.toCollectionModel(entities);
	}

	private Collection<?> emptyCollection() {
		return Collections.singletonList(new EmbeddedWrappers(false).emptyCollectionOf(getResourceType()));
	}

}
