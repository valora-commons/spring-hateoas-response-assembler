package dev.valora.commons.springhateoasresponseassembler.reactive;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.PagedModel.PageMetadata;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.EmbeddedWrappers;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public abstract class HateoasResponseAssembler<S, D extends RepresentationModel<D>>
		extends RepresentationModelAssemblerSupport<S, D> {

	protected HateoasResponseAssembler(Class<?> controllerClass, Class<D> resourceType) {
		super(controllerClass, resourceType);
	}

	@SuppressWarnings("unchecked")
	public PagedModel<D> toPagedModel(Collection<S> entities, int pageNumber, int pageSize, int totalElements) {
		int totalPages = totalElements / pageSize;
		PageMetadata pageMetadata = new PageMetadata(pageSize, pageNumber, totalElements, totalPages);

		if (totalElements == 0) {
			return (PagedModel<D>) PagedModel.of(emptyCollection(), pageMetadata);
		}

		List<D> assembledEntities = new ArrayList<>(totalElements);
		for (S entity : entities) {
			assembledEntities.add(toModel(entity));
		}
		return PagedModel.of(assembledEntities, pageMetadata);
	}

	@Override
	@SuppressWarnings("unchecked")
	public CollectionModel<D> toCollectionModel(Iterable<? extends S> entities) {
		if (entities == null || !entities.iterator().hasNext()) {
			return (CollectionModel<D>) CollectionModel.of(emptyCollection());
		}
		return super.toCollectionModel(entities);
	}

	private Collection<?> emptyCollection() {
		return Collections.singletonList(new EmbeddedWrappers(false).emptyCollectionOf(getResourceType()));
	}

}
