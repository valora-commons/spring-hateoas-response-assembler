# Spring HATEOAS response assembler

Response assembler for Spring HATEOAS. Handle RepresentationModel with empty list and PagedModel.

## Usage

### Maven

Import the latest `dev.valora.commons:spring-hateoas-response-assembler` artifact into your pom.xml.

```
<dependency>
	<groupId>dev.valora.commons</groupId>
	<artifactId>spring-hateoas-response-assembler</artifactId>
	<version>2.0.0</version>
</dependency>
```

### Spring bean

Create your own `HateoasResponseAssembler` extending `dev.valora.commons.springhateoasresponseassembler.HateoasResponseAssembler` (for example: `ExampleResponseAssembler`).

Declare it in your Spring configuration:

```
@Bean
public HateoasResponseAssembler<Example, ExampleResponse> exampleResponseAssembler(PagedResourcesAssembler<Example> pagedResourcesAssembler) {
	return new ExampleResponseAssembler(pagedResourcesAssembler);
}
```

Import the bean into your Controller classes (`HateoasResponseAssembler<Example, ExampleResponse> responseAssembler`).

And use it to return one or multiple responses in your endpoints: `return responseAssembler.toCollectionModel(examples);`

### Reactive assembler

Create your own `HateoasResponseAssembler` extending `dev.valora.commons.springhateoasresponseassembler.reactive.HateoasResponseAssembler` (for example: `ExampleResponseAssembler`).

Declare it in your Spring configuration:

```
@Bean
public HateoasResponseAssembler<Example, ExampleResponse> exampleResponseAssembler() {
	return new ExampleResponseAssembler();
}
```

## Requirements

* Java 11
* Spring Boot 2.4.5